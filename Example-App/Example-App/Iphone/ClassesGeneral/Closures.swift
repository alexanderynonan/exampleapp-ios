//
//  Closures.swift
//  Example-App
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation
import SwiftyJSON

struct Closures {
    
    //MARK: General
    typealias ErrorMessage              = (_ errorMessage   : String)           -> Void
    typealias Message                   = (_ message        : String)           -> Void
    typealias Success                   = () -> Void
    typealias Image                     = (_ image          : UIImage)   -> Void
    
    //MARK: WebServices
    typealias SuccessResponse           = (_ response       : JSON,_ status : Int) -> Void
    typealias FailureResponse           = (_ errorMessage   : String,_ status : Int) -> Void

    //MARK: Login
    typealias Login                     = (_ objUser        : UserBE)   -> Void
    
}



