//
//  WebServicesURL.swift
//  Example-App
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation
import UIKit

struct WebServicesURL {
    
    static private let baseURLServices = "https://devperu.xyz/api/v1/"    
    
    struct LoginURL {
        static let login            = baseURLServices + "auth/login"
        static let logout           = baseURLServices + "auth/logout"
    }
}
