//
//  Constantes.swift
//  Example-App
//
//  Created by Alexander Ynoñan H. on 6/01/22.
//

import Foundation

struct MessageAlert {
    
    struct General {
        
        static let titleButton              = "One Canal" //Nombre del app
        static let upsButton                = "UPS!"
        static let sessionExpire            = "Sesión caducada"
        static let cancelButton             = "Cancelar"
        static let agreeButton              = "Aceptar"
        static let understoodButton         = "Entendido"
        static let errorServiceParse        = "Problemas al extraer los datos"
        static let errorService             = "Problemas al extraer los datos"
        static let errorServiceNotFount     = "Problemas con el servicio"
        static let errorServiceAccount      = "Intentar de nuevo"
        static let errorSession             = "Sesión expirada"
        static let errorValidation          = "Problemas con la validación"
    }
    
    struct Login {
        static let errorUser        = "Ingrese correctamente su numero de documento"
        static let errorTypeDoc     = "Seleccione un tipo de documento"
        static let errorToken       = "Token Invalido"
        static let errorPassword    = "Ingrese correctamente su contraseña"
        static let errorTerminos    = "Acepte los terminos y condiciones"
        static let validatePassword = "Contraseñas no coinciden"
    }
    
}
