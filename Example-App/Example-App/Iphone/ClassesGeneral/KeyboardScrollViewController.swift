//
//  KeyboardScrollViewController.swift
//  Example-App
//
//  Created by Alexander Ynoñan H. on 14/01/22.
//

import UIKit

class KeyboardScrollViewController: UIViewController {
    
    @IBOutlet weak private var constraintButtom   : NSLayoutConstraint!
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.willApperKeyboard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.WillDisappearKeyboard()
    }

}

//MARK: -Keyboard
extension KeyboardScrollViewController {
    
    private func willApperKeyboard(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShown(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func WillDisappearKeyboard(){
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func keyboardWillShown(_ notification: Notification){
        let kbSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect ?? .zero
        UIView.animate(withDuration: 0.35) {
            self.constraintButtom.constant = kbSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc private func keyboardWillBeHidden(_ notification: Notification){
        UIView.animate(withDuration: 0.35, animations: {
            self.constraintButtom?.constant = 10
            self.view.layoutIfNeeded()
        })
    }
}
