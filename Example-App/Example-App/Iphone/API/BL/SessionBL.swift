//
//  SessionBL.swift
//  Example-App
//
//  Created by Alexander Ynoñan H. on 10/01/22.
//

import UIKit
import SwiftyJSON
import CSUtilities

class SessionBL: NSObject {

    static var sharedSession = SessionBL()
    
//    Get session
    func getSession() -> UserBE? {
        
        let defaults = UserDefaults.standard
        
        if let token            = defaults.value(forKey: "token") as? String,
           let name             = defaults.value(forKey: "name") as? String,
           let lastName         = defaults.value(forKey: "lastName") as? String,
           let email            = defaults.value(forKey: "email") as? String,
           let document         = defaults.value(forKey: "document") as? String,
           let salary           = defaults.value(forKey: "salary") as? String,
           let available_salary = defaults.value(forKey: "available_salary") as? String,
           let state_salary     = defaults.value(forKey: "salary_view") as? Bool,
           let document_type    = defaults.value(forKey: "document_type") as? String{
            
            let obj         = UserBE()
            obj.token       = token
            obj.name        = name
            obj.lastName    = lastName
            obj.email       = email
            obj.document    = document
            obj.salary      = salary
            obj.type_doc    = document_type
            obj.state_salary = state_salary
            obj.available_salary = available_salary
            
            do {
                let json = try defaults.getObjectJSON(forKey: "data", castTo: JSON.self)
                let objUser = UserBE(json)
                obj.business = objUser.business
                obj.accounts = objUser.accounts
            } catch {
                print(error.localizedDescription)
            }
            
            return obj
        }else{
            return nil
        }
    }
    
//  Save session
    func saveSession(obj : UserBE){
        
        self.deleteSession()
        
        let defaults = UserDefaults.standard
        
        defaults.setValue(obj.token, forKey: "token")
        defaults.setValue(obj.name, forKey: "name")
        defaults.setValue(obj.lastName , forKey: "lastName")
        defaults.setValue(obj.email , forKey: "email")
        defaults.setValue(obj.document , forKey: "document")
        defaults.setValue(obj.salary , forKey: "salary")
        defaults.setValue(obj.type_doc , forKey: "document_type")
        defaults.setValue(obj.available_salary,forKey: "available_salary")
        defaults.setValue(obj.state_salary,forKey: "salary_view")
        
        do {
            try defaults.setObjectJSON(obj.json_User, forKey: "data")
        } catch {
            print(error.localizedDescription)
        }
        
        defaults.synchronize()
    }

//  Delete session
    func deleteSession(){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "token")
        defaults.removeObject(forKey: "name")
        defaults.removeObject(forKey: "lastName")
        defaults.removeObject(forKey: "email")
        defaults.removeObject(forKey: "document")
        defaults.removeObject(forKey: "document_type")
        defaults.removeObject(forKey: "salary")
        defaults.removeObject(forKey: "data")
        defaults.removeObject(forKey: "available_salary")
        defaults.removeObject(forKey: "salary_view")
        defaults.synchronize()
    }
    
}
