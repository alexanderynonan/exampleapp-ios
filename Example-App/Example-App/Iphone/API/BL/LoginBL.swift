//
//  LoginBL.swift
//  Example-App
//
//  Created by Alexander Ynoñan H. on 10/01/22.
//

import Foundation
import Alamofire

struct LoginBL {
    
    static let shared = LoginBL()
    

    func getLogin(objUser : UserBE,completionService : @escaping Closures.Login, errorService : @escaping Closures.FailureResponse) {
        
        if objUser.type_doc == "Tipo de documento"{
            errorService(MessageAlert.Login.errorTypeDoc,0)
            return
        }
        
        if objUser.document?.count ?? 0 <= 7{
            errorService(MessageAlert.Login.errorUser,0)
            return
        }
        
        if objUser.password?.count ?? 0 <= 4{
            errorService(MessageAlert.Login.errorPassword,0)
            return
        }
        
        LoginWS.shared.getLogin(params: objUser.dic_login, completion: completionService, errorService: errorService)        
    }
    
    
    func sendLogout(completionService : @escaping Closures.Success, errorService : @escaping Closures.FailureResponse) {
        
        if let session = SessionBL.sharedSession.getSession(){
            LoginWS.shared.sendLogout(header: session.headerToken , completionService: completionService, errorService: errorService)
        }else{
            errorService(MessageAlert.General.sessionExpire,0)
        }
    }
    
    
    
}
