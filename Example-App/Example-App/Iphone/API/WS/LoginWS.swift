//
//  LoginWS.swift
//  One Canal
//
//  Created by Alexander Ynoñan H. on 10/01/22.
//

import Foundation
import Alamofire

struct LoginWS {
    
    static let shared = LoginWS()
    
    func getLogin(params dic : [String : Any],completion : @escaping Closures.Login,errorService : @escaping Closures.FailureResponse){
        
        let url = WebServicesURL.LoginURL.login
        
        CSWebservice.sharedInstace.requestType(type: .post, url: url, params: dic, header: nil) { response, status in
            CSWebservice.sharedInstace.genericResponseData(response, statusCode: status, success: { json in
                completion(UserBE(json))
            }, error: errorService)
        }
    }
    
    func sendLogout(header : HTTPHeaders,completionService : @escaping Closures.Success, errorService : @escaping Closures.FailureResponse) {
        
        let url = WebServicesURL.LoginURL.logout
        
        CSWebservice.sharedInstace.requestType(type: .post, url: url, params: nil, header: header) { response, status in

            CSWebservice.sharedInstace.genericResponseData(response, statusCode: status, success: { json in
                completionService()
            }, error: errorService)
        }
    }
    
    
}
