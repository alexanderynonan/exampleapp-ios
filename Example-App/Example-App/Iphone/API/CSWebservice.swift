//
//  CSWebservice.swift
//  Example-App
//
//  Created by Alexander Ynoñan H. on 24/12/21.
//

import Foundation
import Alamofire
import SwiftyJSON
import CSUtilities

class CSWebservice {
    
    static let sharedInstace = CSWebservice()
    
    @discardableResult func requestType(type serviceType : HTTPMethod,url : String,params : Parameters? = nil ,header : HTTPHeaders? = nil,successResponse : @escaping Closures.SuccessResponse) -> DataRequest?{
        
        let request = AF.request(url, method: serviceType,parameters: params, headers: header)
        return request.response { response in
            print("\n\n**********************************************************************")
            print("SERVICIO \(serviceType.rawValue): \(url)") // TYPE SERVICE AND URL
            print(JSON(response.data ?? MessageAlert.General.errorServiceNotFount)) // RESPONSE JSON
            successResponse(JSON(response.data ?? ""), response.response?.statusCode ?? 0)
            print("**********************************************************************\n\n")
        }
    }
    
//    MARK: - VALIDATION RESPONSE DATA JSON
    func genericResponseData(_ response: JSON,statusCode: Int ,success: (_ json: JSON) -> Void, error: @escaping Closures.FailureResponse){
        
        if let status = response.dictionary?["code"], status == 200 || status == "200"{ // SERVICE CORRECT
            success(JSON(response.dictionary?["data"] ?? "nil"))
        }else if let expire = response.dictionary?["code"], expire == 401 || expire == "401"{ //EXPIRE SESSION
            return self.isSessionExpired(message: self.manageErrorFromResponse(response))
        }else{
            error(self.manageErrorFromResponse(response),statusCode) //ERROR SERVICE
        }
    }
    
//    MARK: - VALIDATION RESPONSE MESSAGE TEXT
    func genericResponseStatus(_ response: JSON,statusCode: Int ,success: @escaping Closures.Message, error: @escaping Closures.FailureResponse){
        
        if let status = response.dictionary?["code"], status == 200 || status == "200"{ // SERVICE CORRECT
            success(self.manageErrorFromResponse(response))
        }else if let expire = response.dictionary?["code"], expire == 401 || expire == "401"{ //EXPIRE SESSION
            return self.isSessionExpired(message: self.manageErrorFromResponse(response))
        }else{
            error(self.manageErrorFromResponse(response),statusCode) //ERROR SERVICE
        }
    }
//    MARK: - VALIDATION PARAMETER MESSAGE ENGLISH OR SPANISH
    
    private func manageErrorFromResponse(_ response: JSON? = nil) -> String{
        if let message = response?.dictionary?["message"]{
            return message.stringValue
        }else if let message = response?.dictionary?["mensaje"]{
            return message.stringValue
        }else{
            return MessageAlert.General.errorServiceNotFount
        }
    }

//    MARK: - SESSION EXPIRE
    
    private func isSessionExpired(message : String) -> Void{ // RETURN FIRT VIEW CONTROLLER AND SELETE SESSION
        
        guard let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController else { return }
        
        let cancelButton = CSAlertButton(title: MessageAlert.General.understoodButton)
        
        navigationController.showSystemAlert(MessageAlert.General.titleButton, message: message, buttons: [], cancel: cancelButton) { (int) in
        } andCancelHandler: {
            SessionBL.sharedSession.deleteSession() // DELETE SESSION
            navigationController.popToRootViewController(animated: true)
        }
    }
}
