//
//  UserBE.swift
//  One Canal
//
//  Created by Alexander Ynoñan H. on 10/01/22.
//

import UIKit
import SwiftyJSON
import Alamofire

class UserBE {

    static let shared = UserBE()
    
    var document            : String?
    var password            : String?
    
    var type_doc            : String?
    var token               : String?
    var name                : String?
    var lastName            : String?
    var email               : String?
    var salary              : String?
    var available_salary    : String?
    var state_salary        : Bool?
    var business            : UserBusinessBE?
    var accounts            : [UserAccountsBE]?
    
    var json_User   = JSON()
    
    init(_ json : JSON? = nil){
        self.token      = json?.dictionary?["access_token"]?.string
        self.name       = json?.dictionary?["me"]?.dictionary?["names"]?.string
        self.lastName   = json?.dictionary?["me"]?.dictionary?["surnames"]?.string
        self.type_doc   = json?.dictionary?["me"]?.dictionary?["document_type"]?.string
        self.document   = json?.dictionary?["me"]?.dictionary?["document_number"]?.string
        self.salary     = json?.dictionary?["me"]?.dictionary?["salary"]?.string
        self.email      = json?.dictionary?["me"]?.dictionary?["email"]?.string
        self.available_salary  = json?.dictionary?["me"]?.dictionary?["available_salary"]?.string
        self.state_salary      = json?.dictionary?["me"]?.dictionary?["salary_view"]?.boolValue ?? false
        self.business   = UserBusinessBE(json?.dictionary?["me"]?.dictionary?["business"])
        self.accounts   = json?.dictionary?["me"]?.dictionary?["accounts"]?.array?.map({ UserAccountsBE($0) }) ?? []
        self.json_User  = json ?? JSON()
    }
    
    func parseInfo(_ json : JSON? = nil) -> UserBE{
                
        self.name       = json?.dictionary?["names"]?.string
        self.lastName   = json?.dictionary?["surnames"]?.string
        self.type_doc   = json?.dictionary?["document_type"]?.string
        self.document   = json?.dictionary?["document_number"]?.string
        self.salary     = json?.dictionary?["salary"]?.string
        self.email      = json?.dictionary?["email"]?.string
        self.available_salary  = json?.dictionary?["available_salary"]?.string
        self.state_salary      = json?.dictionary?["salary_view"]?.boolValue ?? false
        self.business   = UserBusinessBE(json?.dictionary?["business"])
        self.accounts   = json?.dictionary?["accounts"]?.array?.map({ UserAccountsBE($0) }) ?? []
        
        return self
    }
    
    var dic_login : [String : Any] {
        let dic : [String : Any] = ["document_type" : self.type_doc ?? "",
                                    "document_number" : self.document ?? "",
                                    "password" : self.password ?? ""]
        return dic
    }
    
    var dic_validate : [String : Any] {
        let dic : [String : Any] = ["document_type" : self.type_doc ?? "",
                                    "document_number" : self.document ?? ""]
        return dic
    }
    
    var headerToken : HTTPHeaders {
        let dic = ["Authorization" : "Bearer \( self.token ?? "")",
                   "Accept" : "application/json"]
        return HTTPHeaders(dic)
    }
    
    var dic_passwordAccount : [String : Any] {
        let dic : [String : Any] = ["access_token" : self.token ?? "",
                                    "password_confirmation" : self.password_repeat ?? "",
                                    "password" : self.password ?? ""]
        return dic
    }
    
    var dic_recoverPassword : [String : Any] {
        let dic : [String : Any] = ["current_password" : self.password_actual ?? "",
                                    "password_confirmation" : self.password_repeat ?? "",
                                    "password" : self.password ?? ""]
        return dic
    }
    
    //ACCOUNT ACTIVATION
    
    var password_repeat : String?
    var password_actual : String?
}

class UserBusinessBE {
    
    var id : Int?
    var ruc : String?
    var name : String?
    
    init(_ json : JSON? = nil) {
        self.id     = json?.dictionary?["id"]?.int
        self.ruc    = json?.dictionary?["ruc"]?.string
        self.name   = json?.dictionary?["name"]?.string
    }
}

class UserAccountsBE {
    
    var id : Int?
    var number : String?
    var active : Int?
    var bank_id : Int?
    var objBank : UserBankBE?
    
    init(_ json : JSON? = nil) {
        self.id         = json?.dictionary?["id"]?.int
        self.number     = json?.dictionary?["number"]?.string
        self.active     = json?.dictionary?["active"]?.int
        self.bank_id    = json?.dictionary?["bank_id"]?.int
        self.objBank    = UserBankBE(json?.dictionary?["bank"])
    }
    
    var name_select : String {
        if id == -1{
            return "Seleccionar cuenta de depósito"
        }else{
            return "\(self.objBank?.shortname ?? "") - \(self.number ?? "")"
        }
        
    }
}

class UserBankBE {
    
    var id : Int?
    var shortname : String?
    var name : String?
    
    init(_ json : JSON? = nil) {
        self.id         = json?.dictionary?["id"]?.int
        self.shortname  = json?.dictionary?["short_name"]?.string
        self.name       = json?.dictionary?["name"]?.string
    }
}
