//
//  ViewController.swift
//  Example-App
//
//  Created by Alexander Ynoñan H. on 25/03/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak private var lblTitulo        : UILabel!
    @IBOutlet weak private var txtName          : UITextField!
    @IBOutlet weak private var txvDescripcion   : UITextView!
    @IBOutlet weak private var btnEnviar        : UIButton!
    @IBOutlet weak private var viewHeader       : UIView!
    @IBOutlet weak private var tblAlumnos       : UITableView!
    @IBOutlet weak private var clvAlumnos       : UICollectionView!
    @IBOutlet weak private var stackAlumnos     : UIStackView!
    
    private var objAlumno : AlumnoBE?
    
    private var arrayAlumnos : [AlumnoBE]?
    private var arrayAlumnosFilter : [AlumnoBE]?
    
//MARK: LifeCyle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadAlumnos()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

}

//MARK: - METHODS
extension ViewController {
    
    private func loadAlumnos(){
        self.lblTitulo.text = "Alumnos"
        self.txtName.text = "Alexander"
        self.txvDescripcion.text = "Alumno del curso de iOS"
        self.btnEnviar.setTitle("Enviar", for: .normal)
        self.btnEnviar.setTitleColor(.red, for: .normal)
        self.viewHeader.backgroundColor = .magenta
    }
    
}

//MARK: - TableView
extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayAlumnos?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = ""
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        return cell
    }
}

//MARK: - CollectionView
extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayAlumnos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = ""
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 100)
    }
}






class AlumnoBE {
    
    var nombre : String?
    var apellido : String?
}
